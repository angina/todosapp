package edu.towson.cosc435.anginashrestha.todos_anginashrestha.models
import android.media.Image
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
data class Todos (
    @PrimaryKey
    var id:UUID,
    @ColumnInfo(name = "title")
    val title: String,
    @ColumnInfo(name = "contents")
    val contents: String,
    @ColumnInfo(name = "is_completed")
    var IsCompleted: Boolean,
    @ColumnInfo(name = "date_created")
    val DateCreated: String= "03/08/2020",
    @ColumnInfo(name = "icon_url")
    @SerializedName("icon_url")
    val iconURL: String
)
{
    override fun toString():String{
    return "\n\nTitle: " + title + "\n\nContents: " + contents + "\n\nIsCompleted: " + IsCompleted.toString() + "\n\nDate Created: " + DateCreated
}
}
