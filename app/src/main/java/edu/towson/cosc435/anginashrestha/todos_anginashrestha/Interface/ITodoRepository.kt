package edu.towson.cosc435.anginashrestha.todos_anginashrestha.Interface

import edu.towson.cosc435.anginashrestha.todos_anginashrestha.models.Todos

interface ITodoRepository {
    fun getCount(): Int
    fun getTodo(idx: Int): Todos
    suspend fun getAll(): List<Todos>
    suspend fun remove(todo: Todos)
    suspend fun replace(idx: Int, todo: Todos)
    suspend fun addTodo(todo: Todos)
    fun IsCompleted(todo: Todos)
}