package edu.towson.cosc435.anginashrestha.todos_anginashrestha.Network

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.Interface.ITodoController
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.models.Todos
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import java.net.URL
import java.net.URLConnection
import java.net.URLDecoder
import java.util.*
interface ITodoApi{
    suspend fun fetchTodos(): Deferred<List<Todos>>
    suspend fun fetchIcon(iconUrl: String): Deferred <Bitmap>
}

class TodoApi(val controller: ITodoController): ITodoApi {

    private val BASE_URL: String ="https://my-json-server.typicode.com/rvalis-towson/todos_api/todos"
    private val client: OkHttpClient = OkHttpClient()

    override suspend fun fetchTodos(): Deferred<List<Todos>> {
        return controller.async(Dispatchers.IO) {
            val request = Request.Builder()
                .url(BASE_URL)
                .get()
                .build()
            val result: String? = client.newCall(request).execute().body?.string()
            val todo: List<Todos> = parseJson(result)
            todo
        }
    }

    override suspend fun fetchIcon(iconUrl: String): Deferred<Bitmap> {
        delay(10*2000)
        return controller.async(Dispatchers.IO) {
            val filename = getImageFilename(iconUrl)
            val bitmap = controller.checkCache(filename)
            if(bitmap != null) {
                bitmap
            } else {
                val request = Request.Builder()
                    .url(iconUrl)
                    .get()
                    .build()
                val stream = client.newCall(request).execute().body?.byteStream()
                val bitmap = BitmapFactory.decodeStream(stream)
                if(bitmap != null)
                    controller.cacheIcon(filename, bitmap)
                bitmap
            }
        }
    }
    private  fun parseJson (json: String?): List<Todos>{
        val todos = mutableListOf<Todos>()
        if (json == null) return todos
        val jsonArr = JSONArray(json)
        var i =0
        while (i<jsonArr.length()){
            val jsonObj = jsonArr.getJSONObject(i)
            val todo = Todos(
                id = UUID.fromString(jsonObj.getString("id")),
                title = jsonObj.getString("title"),
                contents = jsonObj.getString("contents"),
                IsCompleted = jsonObj.getBoolean("completed"),
                iconURL = jsonObj.getString("image_url")
            )
            todos.add(todo)
            i++
        }
        return todos
    }
   // private var editingTodoIdx: Int = 0
    private fun getImageFilename(url: String): String {
            try{
                val urlObj = URL(url)
                val query = urlObj.query
                val filename = query.plus(".jpg")
                return filename

                return query.toString()

            }catch (e: Exception){
                return ""
            }
        }
}