package edu.towson.cosc435.anginashrestha.todos_anginashrestha.Interface

import android.graphics.Bitmap
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.models.Todos

import kotlinx.coroutines.CoroutineScope

interface ITodoController : CoroutineScope {
    suspend fun deleteTodo(todo: Todos)
    suspend fun addNewTodo(todo: Todos)
    suspend fun handleEditedTodo(todo: Todos)
    suspend fun fetchTodos(): List<Todos>
    suspend fun fetchIcon(url: String): Bitmap
    suspend fun checkCache(icon: String): Bitmap?
    suspend fun cacheIcon(filename: String, icon: Bitmap)
    fun launchAddTodoScreen()
    val todos: ITodoRepository
    fun editTodo(idx: Int)
    fun completed(todo: Todos)
    fun clearEditingTodo()
    fun getCurrentCount(): Int
    fun getTodoForEdit(): Todos?

}

