package edu.towson.cosc435.anginashrestha.todos_anginashrestha

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationManagerCompat
import android.os.Bundle
import android.os.MessageQueue
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.database.TodoDatabase
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.database.TodoDatabaseRepository
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.fragments.AddTodoFragment
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.fragments.TodoListFragment
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.Interface.ITodoController
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.Interface.ITodoRepository
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.models.Todos
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.Network.ITodoApi
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.Network.TodoApi
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_todo_list.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.lang.Exception
import java.util.*
import kotlin.coroutines.CoroutineContext


class MainActivity : AppCompatActivity(), ITodoController {
    override lateinit var todos: ITodoRepository
    private var editingTodo: Todos? = null
    private var editingTodoIdx: Int = -1
    private lateinit var todoApi: ITodoApi

    private fun showSpinner(){
        progressBar.visibility = View.VISIBLE
    }

    private fun hideSpinner(){
        progressBar.visibility = View.GONE
    }

    override fun launchAddTodoScreen() {
        if(resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            findNavController(R.id.nav_host_fragment)
                .navigate(R.id.action_todoListFragment_to_addTodoFragment)
        }
    }

    override suspend fun addNewTodo(todo: Todos) {
        showSpinner()
        try{
            withContext(Dispatchers.IO){
                todos.addTodo(todo)
            }
            if(resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
                findNavController(R.id.nav_host_fragment)
                    //hereeee
                    .popBackStack()
            }else{
                recyclerView.adapter?.notifyDataSetChanged()
            }
        }catch (e: Exception){
            Log.e(TAG, "Error: ${e.message}")
         //   Toast(this, "Failed to add new todo", Toast.LENGTH_SHORT).show()
            throw e
        }finally {
            hideSpinner()
        }
    }

    override suspend fun deleteTodo(todo: Todos) {
        showSpinner()
        try {
            withContext(Dispatchers.IO) {
                todos.remove(todo)
            }
        } catch (e: Exception) {
            Log.e(TAG, "Error ${e.message}")
            Toast.makeText(this, "Failed to delete song", Toast.LENGTH_SHORT).show()
            throw e
        } finally {
            hideSpinner()
        }

    }
    override fun completed(todo: Todos) {
        todos.IsCompleted(todo)
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun editTodo(idx: Int) {
        val todo = todos.getTodo(idx)
        editingTodo = todo
        editingTodoIdx = idx
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            findNavController(R.id.nav_host_fragment)
                .navigate(R.id.action_todoListFragment_to_addTodoFragment)
        }
        else{
            // landscape
            (addTodoFragment as AddTodoFragment).populateTodo()
        }
    }

    override fun getTodoForEdit(): Todos? {
        return editingTodo
    }

    override fun clearEditingTodo() {
        editingTodo = null
        editingTodoIdx = -1
    }

    override val coroutineContext: CoroutineContext
        get() = lifecycleScope.coroutineContext

    override suspend fun handleEditedTodo(todo: Todos) {
        showSpinner()
        try {
            todos.replace(editingTodoIdx, todo)
            if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
                findNavController(R.id.nav_host_fragment)
                    .popBackStack()
            }else{
                recyclerView.adapter?.notifyItemChanged(editingTodoIdx)
            }
            clearEditingTodo()
        }catch (e:Exception){
            Log.e(TAG, "Error: $(e.message)")
            Toast.makeText(this, "Failed to update Todo", Toast.LENGTH_SHORT).show()
            throw e
        }finally {
            hideSpinner()
        }
    }

    override suspend fun fetchTodos(): List<Todos> {
        return todoApi.fetchTodos().await()
    }

    override suspend fun fetchIcon(url: String): Bitmap {
        return todoApi.fetchIcon(url).await()
    }

    override suspend fun checkCache(icon: String): Bitmap? {
        val file = File(cacheDir, icon)
        if (file.exists()){
            val input = file.inputStream()
            return BitmapFactory.decodeStream(input)
        }else{
            return null
        }
    }

    override suspend fun cacheIcon(filename: String, icon: Bitmap) {
        val file = File(cacheDir, filename)
        val output = file.outputStream()
        icon.compress(Bitmap.CompressFormat.JPEG, 100, output)
    }

    override fun getCurrentCount(): Int {
        return todos.getCount()
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        todoApi = TodoApi(this)
        todos = TodoDatabaseRepository(this)


        launch {
            val TodoListAPI = fetchTodos()
            val TodoListDB = todos.getAll()
            TodoListAPI.forEach { todoAPI ->
                if(TodoListDB.firstOrNull { songDB -> songDB.id == todoAPI.id } == null) {
                    todos.addTodo(todoAPI)
                }
            }
        }


    }
    override fun onStop(){
        super.onStop()
        this.cancel()
    }

    companion object{
        val TAG = MainActivity::class.java.simpleName
        val JOB_ID = 1

    }
}


