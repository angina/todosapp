package edu.towson.cosc435.anginashrestha.todos_anginashrestha

import edu.towson.cosc435.anginashrestha.todos_anginashrestha.Interface.ITodoRepository
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.models.Todos
import java.lang.Exception
import java.util.*

class TodoRepository():ITodoRepository {

    private var todos: MutableList<Todos> = mutableListOf()

   /* init {
        val seed = (1..10).map { idx -> Todos(UUID.randomUUID(), "Title${idx}", "Contents${idx}", idx % 2 == 0, "Just Created") }
        todos.addAll(seed)
    }*/

    override fun getTodo(idx: Int): Todos {
        return todos.get(idx)
    }

    override suspend fun getAll(): List<Todos> {
        return todos
    }

    override fun getCount():Int {

        return todos.size
    }

    override suspend fun remove(todo: Todos) {
      todos.remove(todo)
    }

    override suspend fun replace(idx: Int, todo: Todos) {
        if(idx >= todos.size){
            throw Exception("Out of bounds")
        }
        todos[idx] = todo
    }

    override suspend fun addTodo(todo: Todos) {
        todos.add(todo)
    }

    override fun IsCompleted(todo: Todos) {
        val check = !todo.IsCompleted
        todo.IsCompleted = check
        var position = 0
        val id = todo.id

        while (position < todos.size){
            if (todos[position].id == id){
                todos[position].IsCompleted = check
                break
            }
            position++
        }
    }

}