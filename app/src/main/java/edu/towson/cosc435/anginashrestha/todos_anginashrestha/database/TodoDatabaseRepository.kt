package edu.towson.cosc435.anginashrestha.todos_anginashrestha.database

import android.content.Context
import androidx.room.Room
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.Interface.ITodoRepository
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.models.Todos
import kotlinx.coroutines.delay
import java.lang.Exception

class TodoDatabaseRepository(ctx: Context) : ITodoRepository {
    private val todoList: MutableList<Todos> = mutableListOf()
    private val db: TodoDatabase

    init{
        db = Room.databaseBuilder(
            ctx,
            TodoDatabase::class.java,
        "todo.db"
        ).build()
       // refreshTodoList()
    }
    override fun getCount(): Int {
        return todoList.size
    }

    override fun getTodo(idx: Int): Todos {
        return todoList.get(idx)
    }

    override suspend fun getAll(): List<Todos> {
        if(todoList.size==0){
            refreshTodoList()
        }
        return todoList
    }

    override suspend fun remove(todo: Todos) {

        if (System.currentTimeMillis() % 2 == 0L) throw Exception()
        db.todoDao().deleteTodo(todo)
        refreshTodoList()
    }

    override suspend fun replace(idx: Int, todo: Todos) {

       db.todoDao().updateTodo(todo)
        refreshTodoList()
    }

    override suspend fun addTodo(todo: Todos) {

        db.todoDao().addTodo(todo)
        refreshTodoList()
    }

    override fun IsCompleted(todo: Todos) {
        val check = !todo.IsCompleted
        todo.IsCompleted = check
        var position = 0
        val id = todo.id

        while (position < todoList.size){
            if (todoList[position].id == id){
                todoList[position].IsCompleted = check
                break
            }
            position++
        }
    }

    private suspend fun refreshTodoList(){
        todoList.clear()
        val todo = db.todoDao().getAllTodo()
        todoList.addAll(todo)
    }
}
