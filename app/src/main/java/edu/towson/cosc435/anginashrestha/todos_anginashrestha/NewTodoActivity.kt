package edu.towson.cosc435.anginashrestha.todos_anginashrestha

import android.app.Activity
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.models.Todos
import kotlinx.android.synthetic.main.activity_new_todo.*
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

class NewTodoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_todo)

        savebtn.setOnClickListener{onClick()}

    }


    private fun onClick(){
        when(null){
            R.id.savebtn -> {
                val titletest = changetitle.editableText.toString()
                val contentstest = content.editableText.toString()
                if(titletest.isEmpty() || contentstest.isEmpty())
                Toast.makeText(this, "Title or Content is missing",Toast.LENGTH_SHORT).show()
                else {
                    val intent = Intent()
                    val isCompleted = isCompleted.isChecked
                    val title = changetitle.text.toString()
                    val content = content.text.toString()
                    val exact = Calendar.getInstance()
                    val date = SimpleDateFormat("EEE MMMM d H:m:s Y").format(exact.time)
                    val iconURL = ""
                    val todos = Todos(UUID.randomUUID(),title, content, isCompleted, date, iconURL)
                    val json = Gson().toJson(todos)
                    intent.putExtra(TODO_KEY, json)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
            }
        }
    }
    companion object{
        val TODO_KEY = "TODO"
    }

}