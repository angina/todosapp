package edu.towson.cosc435.anginashrestha.todos_anginashrestha

import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.Interface.ITodoController
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.models.Todos
import kotlinx.android.synthetic.main.todo_view.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class TodosAdapter(val controller: ITodoController) : RecyclerView.Adapter<TodoViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.todo_view, parent, false)
        val viewHolder = TodoViewHolder(view)

        view.setOnLongClickListener(){
            val position = viewHolder.adapterPosition
            val dialog = AlertDialog.Builder(view.context)
            val todo = controller.todos.getTodo(position)

            dialog.setMessage(" Are you sure you want to delete?")
                .setCancelable(false)
                .setPositiveButton("Proceed", DialogInterface.OnClickListener{
                    dialog, id -> controller.launch{
                    try{
                        controller.deleteTodo(todo)
                        this@TodosAdapter.notifyItemRemoved(position)
                    }catch (e:Exception){

                    }
                }

                })
                .setNegativeButton("Cancel", DialogInterface.OnClickListener{
                    dialog, id -> dialog.cancel()
                })

            val userAlert = dialog.create()
            userAlert.setTitle(controller.todos.getTodo(position).title)
            userAlert.show()
            return@setOnLongClickListener true
        }
        view.isCompleted.setOnClickListener {
            view.alpha = 0.5f
            val position = viewHolder.adapterPosition
            controller.launch{
                try{
                    controller.completed(controller.todos.getTodo(position))
                    this@TodosAdapter.notifyItemChanged(position)
                }catch (e: Exception) {
                    view.alpha = 1.0f
                    throw e
                }
            }
        }
        view.setOnClickListener{
            val position = viewHolder.adapterPosition
            val idx = position
            controller.launch{
                try{
                    controller.editTodo(position)
                }catch (e: Exception){

                }
            }
        }
        return viewHolder
    }

    override fun getItemCount(): Int {
        return controller.getCurrentCount()
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        val todo = controller.todos.getTodo(position)
        holder.BindTodo(controller, todo)
    }
}

class TodoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun BindTodo(controller: ITodoController, todo: Todos?) {
        if (todo!!.IsCompleted) {
            itemView.titleview_txt.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            itemView.todoview_txt.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
        }
        else {
            itemView.titleview_txt.paintFlags = 0
            itemView.todoview_txt.paintFlags = 0
        }
        if (todo.contents.length > 100){
            itemView.todoview_txt.text = todo.contents.subSequence(0,99)
        }
        else{
            itemView.todoview_txt.text = todo.contents
        }

        if (todo.title.length > 100){
            itemView.titleview_txt.text = todo.title.substring(0, 99)
        }
        else{
            itemView.titleview_txt.text = todo.title
        }


        controller.launch(Dispatchers.Main) {
            val bitmap = controller.fetchIcon(todo.iconURL)
            itemView.todoIconImage.setImageBitmap(bitmap)
            itemView.iconProgress.visibility = View.INVISIBLE
            itemView.todoIconImage.visibility = View.VISIBLE
            }


        val today = Calendar.getInstance()
        itemView.dateview_txt.text = SimpleDateFormat("EEE MMMM d H:m:s Y").format(today.time)
        itemView.isCompleted.isChecked = todo.IsCompleted
        itemView.iconProgress.visibility = View.VISIBLE
        itemView.todoIconImage.visibility = View.INVISIBLE
        }
    }
