package edu.towson.cosc435.anginashrestha.todos_anginashrestha.database

import androidx.lifecycle.LiveData
import androidx.room.*
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.models.Todos
import java.util.*

@Dao
interface TodoDao{
    @Insert
    suspend fun addTodo(todo: Todos)

    @Update
    suspend fun updateTodo(todo: Todos)

    @Delete
    suspend fun deleteTodo(todo: Todos)


    @Query("select id, title, contents, is_completed, date_created, icon_url from Todos")
    suspend fun getAllTodo(): List<Todos>

    @Query("select * from Todos WHERE title LIKE :query")
    suspend fun getATodo(query: String): Todos?
}


class UUIDConverter{
    @TypeConverter
    fun fromString(uuid:String): UUID {
        return UUID.fromString(uuid)
    }
    @TypeConverter
    fun toString(uuid: UUID): String{
        return uuid.toString()
    }
}

@Database(entities = arrayOf(Todos::class), version = 2, exportSchema = false)
@TypeConverters(UUIDConverter::class)
abstract class TodoDatabase : RoomDatabase(){
    abstract fun todoDao(): TodoDao
}