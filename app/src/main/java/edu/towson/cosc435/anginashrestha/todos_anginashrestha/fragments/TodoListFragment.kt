package edu.towson.cosc435.anginashrestha.todos_anginashrestha.fragments


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import androidx.recyclerview.widget.LinearLayoutManager
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.R
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.TodosAdapter
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.Interface.ITodoController
import kotlinx.android.synthetic.main.fragment_todo_list.*
import kotlinx.coroutines.launch
import java.lang.Exception
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class TodoListFragment : Fragment() {

    private lateinit var todoController: ITodoController

    override fun onAttach(context: Context) {
        super.onAttach(context)

        when (context) {
            is ITodoController -> todoController = context
            else -> throw Exception("ITodoController expected")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_todo_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = TodosAdapter(todoController)

        recyclerView.adapter = adapter

        recyclerView.layoutManager = LinearLayoutManager(context)

        addTodo?.setOnClickListener { todoController.launchAddTodoScreen() }

        val deletedTod: String? = null
        val touchHelper = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(
            ItemTouchHelper.UP or ItemTouchHelper.DOWN or ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT,
            1
        ) {
            override fun onMove(

                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                todoController.launch {
                    val sourcePosition: Int = viewHolder.adapterPosition
                    val targetPosition = target.adapterPosition
                    suspend {
                        Collections.swap(
                            todoController.todos.getAll(),
                            sourcePosition,
                            targetPosition
                        )
                    }
                    recyclerView.adapter?.notifyItemMoved(sourcePosition, targetPosition)
                    adapter.notifyItemMoved(sourcePosition, targetPosition)
                }
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val postion = viewHolder.adapterPosition
                when (direction) {
                    ItemTouchHelper.LEFT -> {
                        deletedTod ?: todoController.todos.getTodo(postion)
                        suspend {
                            todoController.todos.remove(
                                todoController.todos.getTodo(
                                    postion
                                )
                            )
                        }
                        recyclerView.adapter?.notifyItemRemoved(postion)
                        if (deletedTod != null) {
                            Snackbar.make(recyclerView, deletedTod, Snackbar.LENGTH_LONG)
                        }
                    }
                }
            }

        })
        touchHelper.attachToRecyclerView(recyclerView)
    }

    override fun onResume() {
        super.onResume()

        todoController.launch {
            todoController.todos.getAll()
            recyclerView?.adapter?.notifyDataSetChanged()
        }
    }
}


