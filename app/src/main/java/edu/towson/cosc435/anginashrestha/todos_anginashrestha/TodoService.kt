package edu.towson.cosc435.anginashrestha.todos_anginashrestha

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.database.TodoDatabaseRepository
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.models.Todos
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.internal.userAgent
import java.util.*
import kotlin.coroutines.CoroutineContext

class TodoServices : JobService(), CoroutineScope {

    override fun onStopJob(params: JobParameters?): Boolean {
        return true
    }

    override fun onCreate(){
        super.onCreate()
        createNotificationChannel()
    }

    override fun onStartJob(params: JobParameters?): Boolean {
        Log.d(TAG ,"Todos Service job Started")
        //fetch a new Song from the web api ...

        val repo = TodoDatabaseRepository(this)
        launch {
            val todo = Todos(UUID.randomUUID(), "Todo from Service",  "Service",  true,  iconURL = "")
            repo.addTodo(todo)
            val notif = createNotification()
            NotificationManagerCompat.from(this@TodoServices).notify(NOTID_ID, notif)
            MessageQueue.Change.postValue(todo)
        }

        return true
    }

    private fun createNotificationChannel (){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val name = "NewTodosChannel"
            val descriptionTxt = "Notification channel for recieving new todos."
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionTxt
            }

            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun createNotification(): Notification {
        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this,0, intent, PendingIntent.FLAG_ONE_SHOT)
        return NotificationCompat.Builder(this,CHANNEL_ID)
            .setContentText("New Avatar is Available!")
            .setContentTitle("New Todo!")
            .setSmallIcon(android.R.drawable.ic_dialog_info)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .build()
    }
    companion object{
        val TAG = TodoServices::class.java.simpleName
        val CHANNEL_ID = "NewTodoChannelId"
        val NOTID_ID = 1
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO


}
