package edu.towson.cosc435.anginashrestha.todos_anginashrestha.fragments


import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.R
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.Interface.ITodoController
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.models.Todos
import kotlinx.android.synthetic.main.fragment_add_todo.*
import kotlinx.coroutines.launch
import java.lang.Exception
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class AddTodoFragment : Fragment() {
    private lateinit var todoController: ITodoController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_todo, container, false)
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)

        when (context) {
            is ITodoController -> todoController = context
            else -> throw Exception("ITodoController expected")
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        savebtn.setOnClickListener { handleAddTodoClick() }

        val todo = todoController.getTodoForEdit()
        populateTodoForm(todo)

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun populateTodoForm(todo: Todos?) {
        if (todo != null) {
            clearForm()
            title.editableText.insert(0, todo.title)
            content.editableText.insert(0, todo.contents)
            isCompleted.isChecked = todo.IsCompleted
            val date = LocalDate.now().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL))
            changetitle?.text = "Edit Todo"
            savebtn.text = "Edit Todo"

        } else {
            changetitle?.text = "Add Todo"
            savebtn.text = "Add Todo"
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun clearForm() {
      //  val date = LocalDate.now().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL))

        title.editableText.clear()
        content.editableText.clear()
        isCompleted.isChecked = false
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun handleAddTodoClick() {

        val todo = Todos(
            id = getTodoId(),
            title = title.editableText.toString(),
            DateCreated = LocalDate.now()
                .format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)),
            contents = content.editableText.toString(),
            IsCompleted = isCompleted.isChecked,
            iconURL = ""
        )

        //val todo = Todos(id, title, content, completed, date)


        todoController.launch {
            try {
                if (todoController.getTodoForEdit() == null) {
                    todoController.addNewTodo(todo)
                } else {
                    todoController.handleEditedTodo(todo)
                    savebtn.text = "Add Todo"
                }
                clearForm()
            } catch (e: Exception) {
            }
        }
    }


    @RequiresApi(Build.VERSION_CODES.O)
    fun populateTodo() {
        val todo = todoController.getTodoForEdit()
        populateTodoForm(todo)
    }

    private fun getTodoId(): UUID {
        val id: UUID
        val editTodo = todoController.getTodoForEdit()
        if (editTodo == null) {
            id = UUID.randomUUID()
        } else {
            id = editTodo.id
        }
        return id
    }
}



