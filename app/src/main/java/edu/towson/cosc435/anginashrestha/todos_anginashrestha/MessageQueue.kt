package edu.towson.cosc435.anginashrestha.todos_anginashrestha

import androidx.lifecycle.MutableLiveData
import edu.towson.cosc435.anginashrestha.todos_anginashrestha.models.Todos

class MessageQueue {
    companion object {
        val Change : MutableLiveData<Todos> = MutableLiveData()
    }
}